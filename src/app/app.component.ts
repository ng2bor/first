import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  lname: string = 'djordjevic'

  redTxt = true

  lista = [
    '0NESTO',
    '1NESTO',
    '2NESTO',
    '3NESTO',
    '4NESTO',
    '5NESTO',
    '6NESTO',
  ]

  students = [
    {
      name: 'Milan Djordjevic',
      email: 'miffmedia@gmail.com',
      public: false
    },
    {
      name: 'Aleksandar Maculj',
      email: 'aleksandar.maculj@.com',
      public: true
    },
    {
      name: 'Bojan Antic',
      email: 'bojan.anitic@gmail.com',
      public: false
    },
    {
      name: 'Bojan Dimitrijevic',
      email: 'bojan.dimitrijevic@gmail.com',
      public: true
    },
  ]

  zbir = 0
  filterShow = true

  toggleView(show: boolean) {
    //this.zbir = this.saberi(1,2,3)
    // Jedna linija
    /*
    Lorem ipsum dolar sit amet
    */

    this.filterShow = !this.filterShow 

  }

  saberi(a: number, b: number, c: number): number {
    var d = a + b + c
    //console.log(d)
    return d
  }





}
